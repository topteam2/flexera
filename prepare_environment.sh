sudo apt install -y build-essential libssl-dev libffi-dev python3-dev python3-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
deactivate