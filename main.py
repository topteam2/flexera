from openpyxl import load_workbook
import ipaddress
import glob
import pandas as pd
import xlrd

cidr_1 = ipaddress.ip_network('10.0.0.0/8')
cidr_2 = ipaddress.ip_network('172.16.0.0/12')
cidr_3 = ipaddress.ip_network('192.168.0.0/16')

folder_name = 'servers'

def net_type(source_ip, destination_ip):
    s_ip = ipaddress.ip_address(source_ip)
    d_ip = ipaddress.ip_address(destination_ip)    
    return 'BSB Network' if (s_ip in cidr_1 or s_ip in cidr_2 or s_ip in cidr_3) and\
        (d_ip in cidr_1 or d_ip in cidr_2 or d_ip in cidr_3) else 'Internet Traffic'


def check_cols_order(cols):
    return cols.index('Source Host Name') == 0\
        and cols.index('Source') == 1\
        and cols.index('Destination Port') == 2\
        and cols.index('Destination') == 3\
        and cols.index('Destination Host Name') == 4\
        and cols.index('Protocol') == 5


invalid_files = []
for original_file in glob.glob(f'{folder_name}/*.xls'):
    print(f'Processing file {original_file}')

    # Convert xls to xlsx
    workbook = xlrd.open_workbook(original_file, ignore_workbook_corruption=True)
    
    df = pd.read_excel(workbook)
    file_name = f'{original_file}x'
    
    # Check the columns's order
    if not check_cols_order(list(df.columns)):
        invalid_files.append(original_file)
        continue
    df.to_excel(file_name, index=False)

    file_name = f'{original_file}x'
    server_name = file_name.rsplit('.', 1)[0].rsplit('/', 1)[-1].lower()

    wb = load_workbook(file_name)
    ws = wb.worksheets[0]

    # Delete unecessary columns
    ws.delete_cols(7, 4)

    # Create both In/Out and Intranet/Internet columns
    ws.insert_cols(3, 1)
    ws['C1'] = 'In/Out'
    ws['H1'] = 'Intranet/Internet'

    rows_to_delete = set()
    values = []
    for row in ws.iter_rows():
        # Skip header row
        if row[0].coordinate == 'A1':
            continue

        # Populate In/Out column
        row[2].value = 'OUTBOUND' if row[0].value.lower() == server_name.lower() else 'INBOUND'

        # Populate Intranet/Internet column
        row[7].value = net_type(row[1].value, row[4].value)

        # Check for RISC row
        if row[0].value.startswith('RISC_Networks'):
            rows_to_delete.add(row[0].row)

        # Check for duplicate row
        r = {
            'Source': row[1].value,
            'In/Out': row[2].value,
            'Destination Port': row[3].value,
            'Destination': row[4].value}

        if r in values:
            rows_to_delete.add(row[0].row)
        else:
            values.append(r)

        # Check for internal communication row
        if row[0].value.lower() == server_name.lower() and row[5].value.lower() == server_name.lower():
            rows_to_delete.add(row[0].row)

    # Remove invalid rows
    for row in reversed(list(sorted(rows_to_delete))):        
        ws.delete_rows(row, 1)

    #  Sort by Intranet/Internet
    ws.auto_filter.ref = f'A1:H{ws.max_row}'
    ws.auto_filter.add_sort_condition(f'H2:H{ws.max_row}')

    wb.save(file_name)


if invalid_files:
    print()    

for f in invalid_files:
    print(f'WARN: File {f} does not follow the expected column order')

if invalid_files:
    print(f'Expected column order: Source Host Name, Source, Destination Port, Destination, Destination Host Name, Protocol')
